angular.module('starter.controllers', ['ionic', 'ngCordova', 'ngStorage', 'starter.factory'])

.controller('AppCtrl', function($scope, $state, $localStorage, popup, dataRequest) {
    var InactiveSeller = 1;
    var sellerId = $localStorage.IDseller;
    $scope.logout = function(){
		// cordova.plugins.backgroundMode.disable();
		// cordova.plugins.backgroundMode.ondeactivate = function() {
		// 	window.powerManagement.release(function() {
		// 		//console.log('Wakelock released');
		// 	}, function() {
		// 		//console.log('Failed to release wakelock');
		// 	});
		// 	if (angular.isDefined(refreshData)) {
		// 		$interval.cancel(refreshData);
		// 	}
		// }

		delete $localStorage.token;
		delete $localStorage.IDseller;
		ionic.Platform.exitApp();
    }
})

.controller('LoginCtrl',function($scope, $state, $rootScope, $cordovaBarcodeScanner, $localStorage, popup, servGlobal, dataRequest) {
	var ActiveSeller = 2;
    $scope.scanQR = function(){
		//$cordovaBarcodeScanner.scan().then(function(imageData){
			//var sellerId = imageData.text;
			var sellerId = "akbar1311210";
			dataRequest.logInQR(sellerId).then(function(response){
				if(response.status == 12345){
					servGlobal.loadingHide();
					popup.alert("info_first_install");
				}
				else if(response.status == 200){
					servGlobal.loadingHide();
					if(!response.data.sellerId){
						popup.alert("ID Penjual tidak terdaftar");
					}
					else{
						$rootScope.sellerId = sellerId;
						$state.go("app.lists");
					}
				}
			});
		//}, function(error) {
			
		//});
    };
})

.controller('ListsCtrl', function($scope, $state, $ionicPlatform, $ionicModal, $ionicPopup, $ionicLoading, $timeout, $interval, $window, $localStorage, $cordovaLocalNotification, $rootScope, popup, dataRequest, servGlobal) {
	var sellerId = $rootScope.sellerId;
	var processId = 0;
	var doneId = 1;
	var refreshData;
	var ActiveSeller = 2;

	$rootScope.start = function(){
		var orderArr = [];
		refreshData = $interval(function(){
			$scope.isDisabled = true;
			servGlobal.loadingShow();
			dataRequest.getOrderList(sellerId).then(function(response){
				if(!response.data.status || !response.data.body){
					servGlobal.loadingHide();
					servGlobal.loadAuthShow();
					dataRequest.logInQR(sellerId).then(function(response){
						if(response.status == 200){
							if(!response.data.body.sellerId){
								servGlobal.loadingHide();
								popup.alert("ID Penjual tidak terdaftar");
							}
							else{
								$localStorage.IDseller = sellerId;
								dataRequest.setSellerStatus(sellerId, ActiveSeller).then(function(response){
									if(response.status == 200){
										servGlobal.loadingHide();
										if (angular.isDefined(refreshData)) {
											$interval.cancel(refreshData);
										}
										$rootScope.start();
									}
								});
							}
						}
					});
				}
				else{
					servGlobal.loadingHide();
					$scope.test = response.res;
					var counterBookedOrder = 0;
					for(var i = 0; i < response.res.length; i++){
						if (orderArr.length == 0){
							if(response.res[i].orderStatus == 'BOOKED'){
								counterBookedOrder +=1;
							}
							orderArr.push(response.res[i].orderID)
						}
						else{
							var hasItem = false;
							for (var index in orderArr) {
								if(orderArr[index] == response.res[i].orderID){
									hasItem = true;
									break;
								}
							}
							if(hasItem == false){
								if(response.res[i].orderStatus == 'BOOKED'){
									counterBookedOrder +=1;
								}
								orderArr.push(response.res[i].orderID)
							}
						}
					}
					var conterBookedOrderbadge = 0;
					for (var k = 0; k < response.res.length; k++) {
						if (response.res[k].orderStatus == 'BOOKED') {
							conterBookedOrderbadge += 1;
						}
					}
					$scope.countOrder = conterBookedOrderbadge;
					$scope.isDisabled = false;
					// if(counterBookedOrder >0){
					// 	$cordovaLocalNotification.schedule({
					// 		id: "1",
					// 		text: "Ada " +conterBookedOrderbadge+ " Pesanan dengan status BOOKED",
					// 		title: "I-Sell"
					// 	}).then(function () {
					// 	});
					// }					
				}
			});
		}, 10000);
	}
	
	$rootScope.stop = function(){
		if (angular.isDefined(refreshData)) {
			$interval.cancel(refreshData);
		}
	}
	
	dataRequest.getOrderList(sellerId).then(function(response){
		if(!response.data.status || !response.data.body){
			servGlobal.loadAuthShow();
			dataRequest.logInQR(sellerId).then(function(response){
				if(response.status == 200){
					if(!response.data.body.sellerId){
						popup.alert("ID Penjual tidak terdaftar");
					}
					else{
						$localStorage.IDseller = sellerId;
						dataRequest.setSellerStatus(sellerId, ActiveSeller).then(function(response){
							if(response.status == 200){
								servGlobal.loadingHide();
								if (angular.isDefined(refreshData)) {
									$interval.cancel(refreshData);
								}
								$rootScope.start();
							}
						});
					}
				}
			});
		}
		else{
			$scope.test = response.res;
			var counterBookedOrder = 0;
			for(var i = 0; i<response.res.length; i++){
				if(response.res[i].orderStatus == 'BOOKED') counterBookedOrder += 1;
			}
			$scope.countOrder = counterBookedOrder;	
		}
	});
	
	$rootScope.start();
	
    $scope.getTotal = function(order){
		var total = 0;
		if(order.items.length == null){
			harga = parseInt(order.items.sellingPrice._text);
			hargafloat = parseFloat(harga);
			jumlah = parseInt(order.items.quantity._text);
			total = hargafloat * jumlah;
		}
		else{
			for(var i = 0; i < order.items.length; i++){
				harga = parseInt(order.items[i].sellingPrice._text);
				hargafloat = parseFloat(harga);
				jumlah = parseInt(order.items[i].quantity._text);
				total += hargafloat * jumlah;
			}
		}
		return total;
    };
	
	// cordova.plugins.backgroundMode.enable();
      
    // cordova.plugins.backgroundMode.onactivate = function() {
	// 	window.powerManagement.dim(function() {
	// 		//console.log('Wakelock acquired');
	// 	}, function() {
	// 		//console.log('Failed to acquire wakelock');
	// 	});
		
	// 	window.powerManagement.setReleaseOnPause(false, function() {
	// 		//console.log('setReleaseOnPause successfully');
	// 	}, function() {
	// 		//console.log('Failed to set');
	// 	});

	// 	$ionicPlatform.on('resume', function(){
	// 		$rootScope.stop();
	// 		$rootScope.start();
	// 	})
		
	// 	/*
	// 	$ionicPlatform.on('pause', function(){
	// 		$rootScope.stop();
	// 	});*/
    // }

	$scope.clearSearch = function() {
		$scope.search = "";
	};
	
    $ionicModal.fromTemplateUrl('contact-modal.html', {
		scope: $scope,
		animation: 'slide-in-up'
    }).then(function(modal) {
		$scope.modal = modal;
		$scope.modalDragStart = { active: true, value: 0 }
    });

    $scope.openModal = function(orderID, userID, total) {
		$scope.idOrder = orderID;
		$scope.idBuyer = userID;
		$scope.total = total;
		$scope.modal.show();
    };

    $scope.process = function(idOrder) {
		servGlobal.loadingShow();
		dataRequest.processOrder(idOrder, processId).then(function(response){
			if(!response.data.status || !response.data.body){
				servGlobal.loadingHide();
				popup.alert2("Gagal mengirimkan data.");
			}
			else if(response.data.status.code == 200){
				servGlobal.loadingHide();
				dataRequest.getOrderList(sellerId).then(function(response){
					if(response.data.status.code == 2007){
						//servGlobal.sessionTimeout();
					}
					else{
						if(!response.data.body.GetSellerOrderListResponse){
						}
						else{
							$scope.test = response.res;
							var counterBookedOrder = 0;
							for(var i = 0; i<response.res.length; i++){
								if(response.res[i].orderStatus == 'BOOKED') {
									counterBookedOrder += 1;
								}
								if(response.res[i].orderStatus == 'REFUND' || response.res[i].orderStatus == 'DELIVERED'){
									document.getElementById("openModal1").disabled = true;
								}
							}
							$scope.countOrder = counterBookedOrder;
						}
					}
				});
				$scope.closeModal();
			}
			else{
				popup.alert2("Error");
			}
        }), function(error) {
			// console.log("An error happened -> " + error);
		};
    };

    $scope.ready = function(idOrder) {
		var confirmReady = $ionicPopup.confirm({
			title: 'I-Sell',
			template: "{{'confirm_text'|translate}}"
		});
		var noOrder = idOrder;
		confirmReady.then(function(res) {
			if(res) {
				servGlobal.loadingShow();
				dataRequest.processOrder(noOrder, doneId).then(function(response){
					if(!response.data.status || !response.data.body){
						servGlobal.loadingHide();
						popup.alert2("Gagal mengirimkan data.");
					}
					else if(response.data.status.code == 200){
						servGlobal.loadingHide();
						dataRequest.getOrderList(sellerId).then(function(response){
							if(response.data.status.code == 2007){
								//servGlobal.sessionTimeout();
							}
							else{
								if(!response.data.body.GetSellerOrderListResponse){
								}
								else{
									$scope.test = response.res;
									var counterBookedOrder = 0;
									for(var i = 0; i<response.res.length; i++){
										if(response.res[i].orderStatus == 'BOOKED') counterBookedOrder += 1;
									}
									$scope.countOrder = counterBookedOrder;
								}
							}
						});
						$scope.closeModal();
					}
					else{
						servGlobal.loadingHide();
						popup.alert2("Error");
					}
				}), function(error) {
					servGlobal.loadingHide();
					// console.log("An error happened -> " + error);
				};
			} 
			else {
			}
		});		
    };

    $scope.refund = function(idOrder){
		var confirmRefund = $ionicPopup.confirm({
			title: 'I-Sell',
			template: "{{'confirm_text'|translate}}"
		});
		var noOrder = idOrder;
		confirmRefund.then(function(res) {
			if(res) {
				servGlobal.loadingShow();
				var sellerId = $localStorage.IDseller;
				dataRequest.refundOrder(noOrder).then(function(response){
					if(!response.data.status || !response.data.body){
						servGlobal.loadingHide();
						popup.alert2("Gagal mengirimkan data.");
					}
					else if(response.data.status.code == 200){
						servGlobal.loadingHide();
						popup.alert("success_refund");
						dataRequest.getOrderList(sellerId).then(function(response){
							if(response.data.status.code == 2007){
								//servGlobal.sessionTimeout();
							}
							else{
								if(!response.data.body.GetSellerOrderListResponse){
								}
								else{
									$scope.test = response.res;
									var counterBookedOrder = 0;
									for(var i = 0; i<response.res.length; i++){
										if(response.res[i].orderStatus == 'BOOKED') counterBookedOrder += 1;
									}
									$scope.countOrder = counterBookedOrder;
								}
							}
						});
						$scope.closeModal();
					}
				})
			} 
			else {
			}
		});		
		
    };
	
    $scope.closeModal = function() {
		return $scope.modal.hide();
    };

    $scope.dragDown = function (event) {
		if ($scope.modalDragStart.active) {
			$scope.modalDragStart = { active: false, value: event.gesture.center.pageY };
		}
		var element = angular.element('#modal'),
			windowHeight = $window.innerHeight,
			y = event.gesture.center.pageY - $scope.modalDragStart.value;
		if (y >= 0 && y <= windowHeight) {
			element.css({
				webkitTransform: 'translate3d(0,' + y + 'px,0)',
				transform: 'translate3d(0,' + y + 'px,0)'
			});
		}
    };

    $scope.resetPosition = function (event) {
		$scope.modalDragStart = { active: true, value: 0 };
		var element = angular.element('#modal'),
		y = event.gesture.center.pageY,
        windowHeight = $window.innerHeight;
		if (y >= (windowHeight * 0.5)) {
			element.css({
				transform: 'translate3d(0,' + windowHeight + 'px,0)'
			});
			$timeout(function () {
				$scope.closeModal().then(function () {
					element.removeAttr('style');
				});
			}, 100);
		}
		else {
			$timeout(function () {
				element.removeAttr('style');
			}, 100);
		}
    };
	
	$scope.$on('$destroy', function() {
		$rootScope.stop();
        if (angular.isDefined(refreshData)) {
			$interval.cancel(refreshData);
		}
    });

})

.controller('ReportCtrl', function($scope, $state, $localStorage, popup, dataRequest, servGlobal) {
	var sellerId = $localStorage.IDseller;
	var refreshData;
	var ActiveSeller = 2;
	
	dataRequest.reportOrderList(sellerId).then(function(response){
		if(!response.data.status || !response.data.body){
			servGlobal.loadAuthShow();
			dataRequest.logInQR(sellerId).then(function(response){
				if(response.status == 200){
					if(!response.data.body.sellerId){
						popup.alert("ID Penjual tidak terdaftar");
					}
					else{
						$localStorage.IDseller = sellerId;
						dataRequest.setSellerStatus(sellerId, ActiveSeller).then(function(response){
							if(response.status == 200){
								servGlobal.loadingHide();
								$state.go('app.report');
							}
						});
					}
				}
			});
		}
		else{
			$scope.test = response.res;
			var counterDeliverOrder = 0;
			var counterRefundOrder = 0;
			for(var i = 0; i<response.res.length; i++){
				if(response.res[i].orderStatus == 'DELIVERED') counterDeliverOrder += 1;
			}
			for(var j = 0; j<response.res.length; j++){
				if(response.res[j].orderStatus == 'REFUND') counterRefundOrder += 1;
			}
			$scope.countDeliver = counterDeliverOrder;
			$scope.countRefund = counterRefundOrder;				
		}
	});
	
	
    $scope.getTotal = function(order){
		var total = 0;
		if(order.items.length == null){
			harga = parseInt(order.items.sellingPrice._text);
			hargafloat = parseFloat(harga);
			jumlah = parseInt(order.items.quantity._text);
			total = hargafloat * jumlah;
		}
		else{
			for(var i = 0; i < order.items.length; i++){
				harga = parseInt(order.items[i].sellingPrice._text);
				hargafloat = parseFloat(harga);
				jumlah = parseInt(order.items[i].quantity._text);
				total += hargafloat * jumlah;
			}
		}
		return total;
    };

	$scope.clearSearch = function() {
		$scope.search = "";
	};

})

.controller('PaymentCtrl',function($scope, $state, $ionicHistory, $localStorage, popup, dataRequest, servGlobal) {
    $scope.backToHome = function(){
      $ionicHistory.nextViewOptions({
        disableBack: true
      });

      $state.go('app.lists');
    };
    var sellerId = $localStorage.IDseller;
	var ActiveSeller = 2;
    dataRequest.getPaymentList(sellerId).then(function(response){
      if(!response.data.status || !response.data.body){
		  servGlobal.loadAuthShow();
		  dataRequest.logInQR(sellerId).then(function(response){
				if(response.status == 200){
					if(!response.data.body.sellerId){
						popup.alert("ID Penjual tidak terdaftar");
					}
					else{
						$localStorage.IDseller = sellerId;
						dataRequest.setSellerStatus(sellerId, ActiveSeller).then(function(response){
							if(response.status == 200){
								servGlobal.loadingHide();
								$state.go('app.payment');
							}
						});
					}
				}
			});
      }
      else if(response.data.status.code == 200){
        $scope.transaction = response.res;
      }
      else{
        popup.alert2("Error");
      }
    }), function(error) {
      // console.log("An error happened -> " + error);
    };
})

.controller('SettingsCtrl', function($scope, $state, $ionicHistory) {
	$scope.backToHome = function(){
		$ionicHistory.nextViewOptions({
			disableBack: true
		});
		$state.go('app.lists');
	}
	$scope.lang = function(){
		$ionicHistory.nextViewOptions({
			disableBack: true
		});
		$state.go('app.languages');
	}
})

.controller('LanguageCtrl', function($scope, $state, $translate, $ionicHistory, popup) {
	$scope.changeLang = function(lang){
		$translate.use(lang);
		popup.alert('success_lang_change');
	}
	$scope.backToSettings = function(){
		$ionicHistory.nextViewOptions({
			disableBack: true
		});
		$state.go('app.settings');
	}
})
;
