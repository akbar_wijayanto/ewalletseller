angular.module('starter.factory', ['ui.router', 'ngCordova', 'ngStorage'])

.factory('redirectInterceptor', function($location, $q, $localStorage) {
	return function(promise) {
		promise.then(
			function(response) {
				if (typeof response.data === 'object') {
					if (response.data.status.code == 2007) {
						delete $localStorage.IDseller;
						delete $localStorage.userid;
						delete $localStorage.account;
						delete $localStorage.token;
						$location.path("/login");
					}
				}
				return response;
			},
			function(response) {
				return $q.reject(response);
			}
		);
		return promise;
	};
})

.factory('servGlobal', function($state, $ionicLoading, $cordovaNetwork, $cordovaFile, $rootScope, $localStorage) {
	return{
		checkTimeWaiting:function(statusBooked){
			//var statusBooked = false;
			var timeNow = new Date();
			var timeLast = new Date();
			var time3minutes = timeLast.getTime() + 20000;
			if(timeNow > time3minutes){
				statusBooked = true;
			}
			else{
				statusBooked = false;
			}
			return statusBooked;
		},
		checkSessionApp: function (){
			if (window.localStorage.getItem("auth_token")==null){
				$state.go('login');
			}
		},
		loadingShow: function(){
			$ionicLoading.show({
				template: "<p class='load-icon-left'>Loading...<ion-spinner icon='lines'/></p>"
			});
		},
		loadingHide: function () {
			$ionicLoading.hide();
		},
		loadAuthShow: function(){
			$ionicLoading.show({
				template: "<p class='load-icon-left'>Authenticating...<ion-spinner icon='lines'/></p>"
			});
		},
		sessionTimeout: function (){
			popup.alert("session_timeout");
			delete $localStorage.IDseller;
			delete $localStorage.userid;
			delete $localStorage.account;
			delete $localStorage.token;
			$state.go("login");
		},
		checkConnection:function(){
			var type = $cordovaNetwork.getNetwork();
			var isOnline = $cordovaNetwork.isOnline();
			var isOffline = $cordovaNetwork.isOffline();
			alert("Type : "+type);
			alert("Is Online : "+isOnline);
			alert("Is Offline : "+isOffline);
			document.addEventListener("online", check, false);
			document.addEventListener("offline", checkoff, false);
			function check(){ alert("Internet"); }
			function checkoff(){ alert("No Internet");}
			// listen for Online event
			$rootScope.$on('$cordovaNetwork:online', function(event, networkState){
				var onlineState = networkState;
			});
			// listen for Offline event
			$rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
				var offlineState = networkState;
			})
		},
		checkFile:function(){
			var base = "http://10.2.49.39:8787/mobile-webconsole/apps"; //server final
			var data={'endpoint':[
				{id:"loginQR",url:base+"/sellerLogin"},
				{id:"getOrderList",url:base+"/3/retailAdapter/getSellerOrderListOngoing"},
				{id:"reportOrderList",url:base+"/3/retailAdapter/getSellerOrderListDone"},
				{id:"getPaymentList",url:base+"/3/retailAdapter/getSellerPaymentTrx"},
				{id:"processOrder",url:base+"/3/retailAdapter/processOrder"},
				{id:"processRefund",url:base+"/3/retailAdapter/processRefund"},
				{id:"ActiveInactiveSeller",url:base+"/3/retailAdapter/ActiveInactiveSeller"}
			]};
			// var createFile = function(result){
			// 	link = CryptoJS.AES.encrypt(JSON.stringify(data), "123456");
			// 	$cordovaFile.writeFile(cordova.file.dataDirectory, "JSON/kkas.json",String(link), true);
			// };
			// $cordovaFile.checkDir(cordova.file.dataDirectory, "JSON").then(
			// 	createFile(), function(error){
			// 		 $cordovaFile.createDir(cordova.file.dataDirectory, "JSON", false).then(createFile());
			// 	}
			// )
		}
	}
})

.factory('popup', function($ionicPlatform, $ionicPopup, $ionicLoading, $state, $localStorage) {
	return {
		alert : function(text) {
			$ionicPopup.alert({
				title: 'I-Sell',
				template: "{{'"+text+"'|translate}}",
				okType: 'button-energized'
			});
		},
		alertForceExit : function(text) {
			$ionicPopup.alert({
				title: 'I-Sell',
				template: "{{'"+text+"'|translate}}",
				okType: 'button-energized'
			}).then(function(res) {
				ionic.Platform.exitApp();
			});
		},
		alert2 : function(text) {
			$ionicPopup.alert({
				title: 'I-Sell',
				template: text,
				okType: 'button-energized'
			});
		},
		success : function(text) {
			$ionicPopup.alert({
				title: 'I-Sell',
				template: text,
				okType: 'button-positive'
			});
		}
	}
})

.factory('timestamp',function($filter, $localStorage){
	return{
		show:function(){
			var ref = Date.now();
			var tgl = $filter('date')(ref, 'yyyy-MM-ddTHH:mm:ssZ');
			var tgl_tot=tgl.length;
			var blkng=tgl_tot-2;
			var left=tgl.substr(0,blkng);
			var lenleft=left.length;
			var final_tgl=tgl.substr(lenleft,tgl_tot);
			return(left+":"+final_tgl);
		},
		refnum:function(){
			var ref=0;
			ref=((((Math.random()*10000)+1)+Date.now())*(((Math.random()*10000)+1)+((Math.random()*10000)+1)))/((Math.random()*10000)+1);
			ref=ref.toString().substr(0,8);
			return ref;
		}
	}
})

.factory('jsonOp',function($cordovaFile){
	return{
		getjson:function(file){
			return $cordovaFile.readAsText(cordova.file.dataDirectory, "JSON/"+file).then(
			function(success){
				var decrypted  = CryptoJS.AES.decrypt(success, "123456");
				return{
					'isi':JSON.parse(decrypted.toString(CryptoJS.enc.Utf8))
				}
			})
		},
		updatejson:function(key1,key2,key3,key4,key5,val1,val2){
			return methods.getjson().then(function(response){
				var json=response.isi;
				//alert(key1);
				var jsonl=json[key1][key2].length;
				for(var i=0;i<jsonl;i++){
					if(json[key1][key2][i][key3]==key5){
						json[key1][key2][i][key3]=val1;
						json[key1][key2][i][key4]=val2;
					}
					if (json[key1][key2][i] == key5) {
						json[key1][key2][i] = val1;
					}
				}
				json=CryptoJS.AES.encrypt(JSON.stringify(json), "123456");
				$cordovaFile.writeFile(cordova.file.dataDirectory, "JSON/kkas.json",String(json), true)
				return json;
			});

		},
		insertjson:function(key1,key2,val1,val2){
			return methods.getjson().then(function(response){
				var json=response.isi;
				if(key1 == "nominal"){
					var jsonl=json[key1][key2].length;
					for(var i=0;i<jsonl;i++) {
						var added = json[key1][key2].splice(jsonl, 0, {id: val1, nom: val2});
						if (added != null || added != "") {
							$cordovaFile.writeFile(cordova.file.dataDirectory, "JSON/kkas.json", json, true);
							return json;
						}
					}
				}
				else{
					jsonl=json[key1].length;
					if(key1="rccode") {
						//alert("OPT2");
					//json[key1][val1]=[val2];
					//json[key1].push({val1:val2});
					//json[key1]={val1:val2};
						$cordovaFile.writeFile(cordova.file.dataDirectory, "JSON/kkas.json", json, true);
						return json;
					}
				}
			});
		},
		deletejson:function(key1,key2,key3,key4,key5){
			return methods.getjson().then(function(response){
				var json=response.isi;
				var jsonl=json[key1][key2].length;
				for(var i=0;i<jsonl;i++){
					if(json[key1][key2][i][key3]==key5){
						alert("OPT1");
						alert(i.toString());
						var removed=json[key1][key2].splice(i,1);
						if(removed!=null||removed!=""){
							$cordovaFile.writeFile(cordova.file.dataDirectory, "JSON/kkas.json",json, true);
							return json;
						}
					}
					if (json[key1][key2][i] == key5) {
						alert("OPT2");
						delete json[key1][key2];
						$cordovaFile.writeFile(cordova.file.dataDirectory, "JSON/kkas.json",json, true);
						return json;
					}
				}
			});
		}
	}
})

.factory('dataRequest', function($http, $cordovaDevice, timestamp, jsonOp, popup, servGlobal, $localStorage) {
	var header = null;
	var method = null;
	var url = null;
	var body = null;
	var endpoint = null;
	var deviceOS = null;
	var deviceID = null;
	var osVersion = null;
	var appId = '1';
	var appVersion = '2.0.0';
	var baseUrl = "http://10.2.49.39:8787/mobile-webconsole/apps/retailAdapter/";
	// var data={'endpoint':[
	// 	{id:"loginQR",url:base+"/sellerLogin"},
	// 	{id:"getOrderList",url:base+"/3/retailAdapter/getSellerOrderListOngoing"},
	// 	{id:"reportOrderList",url:base+"/3/retailAdapter/getSellerOrderListDone"},
	// 	{id:"getPaymentList",url:base+"/3/retailAdapter/getSellerPaymentTrx"},
	// 	{id:"processOrder",url:base+"/3/retailAdapter/processOrder"},
	// 	{id:"processRefund",url:base+"/3/retailAdapter/processRefund"},
	// 	{id:"ActiveInactiveSeller",url:base+"/3/retailAdapter/ActiveInactiveSeller"}
	// ]};

	return{
		getServices:function(){
			// jsonOp.getjson("kkas.json").then(function (respond) {
			// 	endpoint = respond.isi.endpoint;
			// });
			deviceOS = $cordovaDevice.getPlatform();
			deviceID = $cordovaDevice.getPlatform();
			osVersion = $cordovaDevice.getPlatform();
		},
		logInQR:function(sellerID){
			servGlobal.loadingShow();
			//if(endpoint==null){
				// servGlobal.checkFile();
				// return jsonOp.getjson("kkas.json").then(function (respond) {
				// 	endpoint = respond.isi.endpoint;
				// 	return {
				// 		'status' : 12345
				// 	}
				// })
			// }
			// else{
				url = baseUrl+"loginSeller";
				method	= "POST";
				header = {'Content-Type':'application/json'};
				body = {
					"metadata" : {
						"datetime" : timestamp.show(),
						"deviceId" : deviceID,
						"devicePlatform" : deviceOS,
						"deviceOSVersion" : osVersion,
						"appId" : appId,
						"appVersion" : appVersion,
						"latitude" : "",
						"longitude" : ""
					},
					"body":{
						"qrcode" : sellerID
					}
				};
				return	$http({method: method, url: url, headers:header, data:body}).then(
					function(response){
						servGlobal.loadingHide();
						if(!response.headers("X-AUTH-TOKEN")){
						}
						else{
							$localStorage.token = response.headers("X-AUTH-TOKEN");
						}

						var loginRes = response.data.body.SellerLoginQRCodeResponse.seller;

						var loginRes = {
							"id" 			 : loginRes.id._text,
							"sellerId" 		 : loginRes.sellerId._text,
							"sellerName" 	 : loginRes.sellerName._text,
							"phoneNumber"  	 : loginRes.phoneNumber._text,
							"streetAddress1" : loginRes.streetAddress1._text,
							"streetAddress2" : loginRes.streetAddress2._text,
							"qrCode"		 : loginRes.qrCode._text
						}
						return{
							'status':response.status,
							'data':loginRes
						};
					},
					function(httpError){
						servGlobal.loadingHide();
						if(httpError.status == '0'){
							popup.alertForceExit("error_no_response");
						}
						else{
							popup.alert2("Status: "+httpError.status+"\n Desc: "+JSON.stringify(httpError.data));
						}
					}
				);
			//}
		},
		getOrderList:function(sellerID){
			url = baseUrl+"getSellerOrderList";
			method	= "POST";
			body = {
				"metadata" : {
					"datetime" : timestamp.show(),
					"deviceId" : deviceID,
					"devicePlatform" : deviceOS,
					"deviceOSVersion" : osVersion,
					"appId" : appId,
					"appVersion" : appVersion,
					"latitude" : "",
					"longitude" : ""
				},
				"body":{
					"sellerId": sellerID
				}
			};
			header = {
				'Content-Type':'application/json',
				'X-AUTH-TOKEN': $localStorage.token
			};
			return	$http({method: method, url: url, headers:header, data:body}).then(
				function(response){
					$localStorage.token = response.headers("X-AUTH-TOKEN");
					var data = {};
					var res= [];
					if(!response.data.body){
						return{
							'status':response.status,
							'data':response.data
						}
					}
					else if(!response.data.body.GetSellerOrderListResponse[0].orders){
						return{
							'status':response.status,
							'data':response.data,
							'res':res,
							'dataStatus':response.data.body.GetSellerOrderListResponse.orders
						}
					}
					else if(response.data.body.GetSellerOrderListResponse[0].orders.constructor === Array){
						for(var i = 0; i < response.data.body.GetSellerOrderListResponse[0].orders.length; i++) {
							data.id = response.data.body.GetSellerOrderListResponse[0].orders[i].id._text;
							data.orderID = response.data.body.GetSellerOrderListResponse[0].orders[i].orderId._text;
							data.orderDate = response.data.body.GetSellerOrderListResponse[0].orders[i].orderDate._text;
							data.orderStatus = response.data.body.GetSellerOrderListResponse[0].orders[i].status._text;
							data.items = response.data.body.GetSellerOrderListResponse[0].orders[i].items;
							data.customer = response.data.body.GetSellerOrderListResponse[0].orders[i].customerName._text;
							data.userID = response.data.body.GetSellerOrderListResponse[0].orders[i].userId._text;
							data.sellerName = response.data.body.GetSellerOrderListResponse[0].orders[i].sellerName._text;
							// data.notes = response.data.body.GetSellerOrderListResponse[0].orders[i].notes._text;
							res.push(data);
							data = {};
						}
					}
					else{
						data.orderID = response.data.body.GetSellerOrderListResponse[0].orders.orderId._text;
						data.orderStatus = response.data.body.GetSellerOrderListResponse[0].orders.status._text;
						data.items = response.data.body.GetSellerOrderListResponse[0].orders.items;
						data.customer = response.data.body.GetSellerOrderListResponse[0].orders.customerName._text;
						data.userID = response.data.body.GetSellerOrderListResponse[0].orders.userId._text;
						//data.notes = response.data.body.GetSellerOrderListResponse[0].orders.notes._text;
						res.push(data);
						data = {};
					}
					return{
						'status':response.status,
						'data':response.data,
						'res':res,
						'dataStatus':response.data.body.GetSellerOrderListResponse.orders
					}
				},
				function(httpError){
					servGlobal.loadingHide();
					if(httpError.status == '0'){
						popup.alertForceExit("error_no_response");
					}
					else{
						popup.alert2("Status : "+httpError.status+"\n Desc : "+JSON.stringify(httpError.data));
					}
				}
			);
		},
		reportOrderList:function(sellerID){
			url = endpoint[2].url;
			method	= "POST";
			body = {
				"metadata" : {
					"datetime" : timestamp.show(),
					"deviceId" : deviceID,
					"devicePlatform" : deviceOS,
					"deviceOSVersion" : osVersion,
					"appId" : appId,
					"appVersion" : appVersion,
					"latitude" : "",
					"longitude" : ""
				},
				"body":{
					"sellerId": sellerID
				}
			};
			header = {
				'Content-Type':'application/json',
				'X-AUTH-TOKEN': $localStorage.token
			};
			return	$http({method: method, url: url, headers:header, data:body}).then(
				function(response){
					$localStorage.token = response.headers("X-AUTH-TOKEN");
					var data = {};
					var res= [];
					if(!response.data.body){
						return{
							'status':response.status,
							'data':response.data
						}
					}
					else if(!response.data.body.GetSellerOrderListDoneResponse.orders){
						return{
							'status':response.status,
							'data':response.data,
							'res':res,
							'dataStatus':response.data.body.GetSellerOrderListDoneResponse.orders
						}
					}
					else if(response.data.body.GetSellerOrderListDoneResponse.orders.constructor === Array){
						for(var i = 0; i < response.data.body.GetSellerOrderListDoneResponse.orders.length; i++) {
							data.orderID = response.data.body.GetSellerOrderListDoneResponse.orders[i].orderId._text;
							data.orderStatus = response.data.body.GetSellerOrderListDoneResponse.orders[i].status._text;
							data.items = response.data.body.GetSellerOrderListDoneResponse.orders[i].items;
							data.customer = response.data.body.GetSellerOrderListDoneResponse.orders[i].customerName._text;
							data.userID = response.data.body.GetSellerOrderListDoneResponse.orders[i].userId._text;
							data.notes = response.data.body.GetSellerOrderListDoneResponse.orders[i].notes._text;
							res.push(data);
							data = {};
						}
					}
					else{
						data.orderID = response.data.body.GetSellerOrderListDoneResponse.orders.orderId._text;
						data.orderStatus = response.data.body.GetSellerOrderListDoneResponse.orders.status._text;
						data.items = response.data.body.GetSellerOrderListDoneResponse.orders.items;
						data.customer = response.data.body.GetSellerOrderListDoneResponse.orders.customerName._text;
						data.userID = response.data.body.GetSellerOrderListDoneResponse.orders.userId._text;
						data.notes = response.data.body.GetSellerOrderListDoneResponse.orders.notes._text;
						res.push(data);
						data = {};
					}
					return{
						'status':response.status,
						'data':response.data,
						'res':res,
						'dataStatus':response.data.body.GetSellerOrderListDoneResponse.orders
					}
				},
				function(httpError){
					servGlobal.loadingHide();
					if(httpError.status == '0'){
						popup.alertForceExit("error_no_response");
					}
					else{
						popup.alert2("Status : "+httpError.status+"\n Desc : "+JSON.stringify(httpError.data));
					}
				}
			);
		},
		getPaymentList:function(sellerID){
			url = baseUrl+"getPaymentList";
			method	= "POST";
			header = {
				'Content-Type':'application/json',
				'X-AUTH-TOKEN': $localStorage.token
			};
			body = {
				"metadata" : {
					"datetime" : timestamp.show(),
					"deviceId" : deviceID,
					"devicePlatform" : deviceOS,
					"deviceOSVersion" : osVersion,
					"appId" : appId,
					"appVersion" : appVersion,
					"latitude" : "",
					"longitude" : ""
				},
				"body":{
					"sellerId": sellerID
				}
			};
			return $http({method: method, url: url, headers:header, data:body}).then(
				function(response){
					delete $localStorage.token;
					$localStorage.token = response.headers("X-AUTH-TOKEN");
					var data = {};
					var res= [];
					if(!response.data.body || !response.data.body.GetSellerPaymentTrxResponse || !response.data.body.GetSellerPaymentTrxResponse.trx){
						return{
							'status':response.status,
							'data':response.data
						};
					}
					else if(response.data.body.GetSellerPaymentTrxResponse[0].trx.constructor === Array){
						for (var i=0; i<response.data.body.GetSellerPaymentTrxResponse[0].trx.length; i++){
							data.transaction1 = response.data.body.GetSellerPaymentTrxResponse[0].trx[i].transactionDate._text;
							data.transaction2 = response.data.body.GetSellerPaymentTrxResponse[0].trx[i].transactionAmount._text;
							data.date = data.transaction1.substr(0, 10);
							data.amount = parseFloat(data.transaction2);
							res.push(data);
							data = {};
						}
					}
					else {
						data.transaction1 = response.data.body.GetSellerPaymentTrxResponse[0].trx.transactionDate._text;
						data.transaction2 = response.data.body.GetSellerPaymentTrxResponse[0].trx.transactionAmount._text;
						data.date = data.transaction1.substr(0, 10);
						data.amount = parseFloat(data.transaction2);
						res.push(data);
						data = {};
					}
					return{
						'status':response.status,
						'data':response.data,
						'res':res
					}
				},
				function(httpError){
					servGlobal.loadingHide();
					if(httpError.status == '0'){
						popup.alertForceExit("error_no_response");
					}
					else{
						popup.alert2("Status : "+httpError.status+"\n Desc : "+JSON.stringify(httpError.data));
					}
				}
			);
		},
		processOrder:function(oID, status){
			url = baseUrl+"processOrder";
			method	= "POST";
			body = {
				"metadata" : {
					"datetime" : timestamp.show(),
					"deviceId" : deviceID,
					"devicePlatform" : deviceOS,
					"deviceOSVersion" : osVersion,
					"appId" : appId,
					"appVersion" : appVersion,
					"latitude" : "",
					"longitude" : ""
				},
				"body":{
					"transactionId": oID,
					"status": status
				}
			};
			header = {
				'Content-Type':'application/json',
				'X-AUTH-TOKEN': $localStorage.token
			};
			return	$http({method: method, url: url, headers:header, data:body}).then(
				function(response){
					delete $localStorage.token;
					$localStorage.token = response.headers("X-AUTH-TOKEN");
					return{
						'status':response.status,
						'data':response.data
					};
				},
				function(httpError){
					servGlobal.loadingHide();
					if(httpError.status == '0'){
						popup.alertForceExit("error_no_response");
					}
					else{
						popup.alert2("Status : "+httpError.status+"\n Desc : "+JSON.stringify(httpError.data));
					}
				}
			);
		},
		refundOrder:function(oID){
			url = endpoint[5].url;
			method	= "POST";
			body = {
				"metadata" : {
					"datetime" : timestamp.show(),
					"deviceId" : deviceID,
					"devicePlatform" : deviceOS,
					"deviceOSVersion" : osVersion,
					"appId" : appId,
					"appVersion" : appVersion,
					"latitude" : "",
					"longitude" : ""
				},
				"body":{
					"orderId" : oID
				}
			};
			header = {
				'Content-Type':'application/json',
				'X-AUTH-TOKEN': $localStorage.token
			};
    		return	$http({method: method, url: url, headers:header, data:body}).then(
				function(response){
					delete $localStorage.token;
					$localStorage.token = response.headers("X-AUTH-TOKEN");
					if(response.data.body.ProcessRefundResponse[0].reference._text != null){
						return{
							'status':response.status,
							'data':response.data,
							'referenceno': response.data.body.ProcessRefundResponse[0].reference._text
						};
					}
					else{
					}
					return{
						'status':response.status,
						'data':response.data
					};
				},
				function(httpError){
					servGlobal.loadingHide();
					if(httpError.status == '0'){
						popup.alertForceExit("error_no_response");
					}
					else{
						popup.alert2("Status : "+httpError.status+"\n Desc : "+JSON.stringify(httpError.data));
					}
				}
			);
		},
		setSellerStatus:function(sellerId, status){
			url = endpoint[6].url;
			method = "POST";
			body = {
				"metadata" : {
					"datetime" : timestamp.show(),
					"deviceId" : deviceID,
					"devicePlatform" : deviceOS,
					"deviceOSVersion" : osVersion,
					"appId" : appId,
					"appVersion" : appVersion,
					"latitude" : "",
					"longitude" : ""
				},
				"body":{
					"sellerId" : sellerId,
					"status" : status
				}
			};
			header = {
				'Content-Type':'application/json',
				'X-AUTH-TOKEN': $localStorage.token
			};
			return	$http({method: method, url: url, headers:header, data:body}).then(
				function(response){
					delete $localStorage.token;
					$localStorage.token = response.headers("X-AUTH-TOKEN");
					return{
						'status':response.status,
						'data':response.data
					};
				},
				function(httpError){
					servGlobal.loadingHide();
					if(httpError.status == '0'){
						popup.alertForceExit("error_no_response");
					}
					else{
						popup.alert2("Status : "+httpError.status+"\n Desc : "+JSON.stringify(httpError.data));
					}
				}
			);
		}
	}
});