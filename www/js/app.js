angular.module('starter', ['ionic', 'ngCordova', 'ngCookies', 'ngStorage', 'starter.controllers', 'pascalprecht.translate',  'starter.factory'])

.run(function($ionicPlatform, $ionicPopup, $localStorage, dataRequest, servGlobal) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
	if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
	if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
	servGlobal.checkFile();
	dataRequest.getServices();
  });
  
   $ionicPlatform.registerBackButtonAction(function(event) {
	  backAsHome.trigger(function(){
    }, function(){ 
    });
  }, 101);
  
})

.config(function($httpProvider, $stateProvider, $urlRouterProvider, $translateProvider, $ionicConfigProvider) {
    $ionicConfigProvider.views.maxCache(0);
	$ionicConfigProvider.scrolling.jsScrolling(false);
	$ionicConfigProvider.views.transition('none');
	$translateProvider.useStaticFilesLoader({
		prefix: 'lang/',
        suffix: '.json'
    });
	$translateProvider.preferredLanguage('id_ID');
    $translateProvider.useLocalStorage();
	$stateProvider
		.state('login', {
			url: '/login',
			templateUrl: 'login.html',
			controller: 'LoginCtrl'
		})
		.state('app', {
			url: '/app',
			abstract: true,
			templateUrl: 'menu.html',
			controller: 'AppCtrl'
		})

		.state('app.payment', {
			url: '/payment',
			views: {
				'menuContent': {
					templateUrl: 'payment.html',
					controller: 'PaymentCtrl'
				}
			}
		})
		.state('app.lists', {
			url: '/lists',
			views: {
				'menuContent': {
					templateUrl: 'lists.html',
					controller: 'ListsCtrl'
				}
			}
		})
		.state('app.report', {
			url: '/report',
			views: {
				'menuContent': {
					templateUrl: 'report.html',
					controller: 'ReportCtrl'
				}
			}
		})
		.state('app.settings', {
			url: "/settings",
			views: {
				'menuContent': {
					templateUrl: 'settings.html',
					controller: 'SettingsCtrl'
				}
			}
		})
		.state('app.languages', {
			url: "/settings/language",
			views: {
				'menuContent': {
					templateUrl: 'language.html',
					controller: 'LanguageCtrl'
				}
			}
		});
	$urlRouterProvider.otherwise('/login');
	$httpProvider.interceptors.push('redirectInterceptor');
});
